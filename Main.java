import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import javafx.geometry.Insets;
import javafx.scene.layout.Pane; 
import javafx.scene.layout.Priority;
import javafx.geometry.Pos;
import javafx.scene.image.Image;

public class Main extends Application
{
    //Javafx Widgets/Elemente
    Button button1;
    Button button2;
    
    
    public static void main(String[] args) {
        CardStack c = new CardStack();
        launch(args);
    }
    
    //Die Klasse "Stage" repräsentiert das Fenster
    @Override   
    public void start(Stage primaryStage) throws Exception {
        //Fenster Titel
        primaryStage.setTitle("BlackJack");
        
        //Fenster Icon
        primaryStage.getIcons().add(new Image("assets/blackjack-icon.png"));
      
        //Buttons initialisieren
        button1 = new Button("Karte nehmen");
        //Event Handler mithilfe von lambda ausdücken. "e" steht für event die Instanz von Eventhandler 
        button1.setOnAction(e -> button1.setText("Karte nehmen!"));
        
        button2 = new Button("Keine Karte");
        button2.setOnAction(e -> button2.setText("Keine Karte!"));
        
        //BorderPane ist eine Layout Klasse aus der Javafx Biblothek
        BorderPane layout = new BorderPane();
        HBox buttonContainer = new HBox();
        
        buttonContainer.getChildren().addAll(button1, button2);
        buttonContainer.setSpacing(20);
        buttonContainer.setMargin(button1, new Insets(0, 0, 100, 0));
        buttonContainer.setMargin(button2, new Insets(0, 0, 100, 0));
        buttonContainer.setAlignment(Pos.BOTTOM_CENTER);
        
        layout.setCenter(buttonContainer);
        
        //Eine Scene Objekt wird erstellt: Der 1.Parameter ist das Layout "layout", welches oben erstellt wurde
        //Der 2.Parameter ist die Weite und der Dritte die Höhe in Pixeln
        Scene scene = new Scene(layout, 1280, 720);
        //Lädt die "style.css"
        scene.getStylesheets().add(this.getClass().getResource("style.css").toExternalForm());
        
        //Die Szene "scene" wird "primaryStage" (dem Fenster) übergeben
        primaryStage.setScene(scene);
        //Zuletzt wird das Fenster auf dem Display gezeigt
        primaryStage.show();
    } 
}
