public class Card
{
    //Mögliche Werte: diamonds, clubs, hearts, spades
    public String suit; //Change to private
    //Mögliche Werte: 2,3,4,5,6,7,8,9,10,Jack,Queen,King,Ace 
    public String value;    //Change to private
    
    
    public Card(String pSuit, String pValue)
    {
        //4 x 13
        this.suit = pSuit;
        this.value = pValue;
    }
}
