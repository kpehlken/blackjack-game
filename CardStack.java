import java.util.Stack;

public class CardStack
{
    private Stack<Card> stack;
    String[] suits = {"diamonds", "clubs", "hearts", "spades"};
    String[] values = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king", "ace"};

    public CardStack()
    {
        stack = new Stack<Card>();
        String suit = suits[0];
        
        //Diese Variable wird benutzt als Index für das "values" Array
        int valueCounter = 0;
        for(int i = 0; i < 52; i++) {
            if(i % 13 == 0 && i != 0) {
                if(i / 13 > 3) {
                    suit = suits[(i / 13) -1];
                } else {
                    suit = suits[(i / 13)];
                }
                valueCounter = 0;
            }

            Card card = new Card(suit, values[valueCounter]);
            valueCounter++;
            stack.push(card);
        }
        
        //Debugging
        for(int j = 0; j < 52; j++) {
            System.out.println(j + " " + stack.peek().suit + " " + stack.peek().value);
            stack.pop();
        }
    }

    
    
    public static void main(String[] args) {
        CardStack c = new CardStack();
    }
    
    
}
